package ru.galeev;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.galeev.model.Product;
import ru.galeev.repository.ProductRepository;

public class Launcher {

    public static void main(String[] args) {
        AbstractApplicationContext context = new ClassPathXmlApplicationContext("config.xml");
        ProductRepository repository = (ProductRepository) context.getBean("repository");

        Product product = new Product();
        product.setName("Sample");

        repository.save(product);

        System.out.println(repository.getAll());
    }
}
