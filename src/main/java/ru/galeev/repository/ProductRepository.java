package ru.galeev.repository;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.galeev.model.Product;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class ProductRepository {

    @Autowired
    private SessionFactory sessionFactory;

    private Session currentSession() {
        return sessionFactory.getCurrentSession();
    }

    public void save(Product product) {
        currentSession().save(product);
    }

    public Product getById(Long id) {
        return (Product) currentSession().get(Product.class, id);
    }

    public List<Product> getAll() {
        Query query = currentSession().getNamedQuery("Product.getAll");
        return query.list();
    }
}